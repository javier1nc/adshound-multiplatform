﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using AdsHound.Models;
using AdsHound.Services;


namespace AdsHound
{
    public partial class HistoryPage : ContentPage
    {

        private SearchService _searchService;
        private List<SearchGroup> _searchGroups;

        public HistoryPage()
        {
            _searchService = new SearchService();
            InitializeComponent();

            PopulateListView(_searchService.GetRecentSearches());
        }

        //protected override async void OnAppearing()
        //{
        //    base.OnAppearing();

        //    // Reset the 'resume' id, since we just want to re-start here
        //    ((App)App.Current).ResumeAtAdId = -1;
        //    listView.ItemsSource = await App.Database.GetItemsAsync();
        //}


        private void PopulateListView(IEnumerable<Search> searches)
        {
            _searchGroups = new List<SearchGroup>
            {
                new SearchGroup("Lugares resientes", searches)
            };

            listView.ItemsSource = _searchGroups;
        }

        void OnSearchTextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            PopulateListView(_searchService.GetRecentSearches(e.NewTextValue));
        }

        void OnRefreshing(object sender, System.EventArgs e)
        {

            PopulateListView(_searchService.GetRecentSearches(searchBar.Text));

            listView.EndRefresh();
        }

        void OnSearchSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var search = e.SelectedItem as Search;

            DisplayAlert("Selecionaste Lugar", search.Location, "OK");
        }

        void OnDeleteClicked(object sender, System.EventArgs e)
        {
            var search = (sender as MenuItem).CommandParameter as Search;


            _searchGroups[0].Remove(search);

            // But we should also update the backend. So, we use SearchService for that.
            _searchService.DeleteSearch(search.Id);
        }

    }
}

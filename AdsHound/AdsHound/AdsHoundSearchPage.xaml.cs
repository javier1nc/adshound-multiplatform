﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

using System.Linq;
using System.Threading.Tasks;
using AdsHound.Services;
using AdsHound.Models;

namespace AdsHound
{
    public partial class AdsHoundSearchPage : ContentPage
    {
        

        private AdService _service = new AdService();

        // Note that IsSearching is a bindable property. This is required for 
        // binding ActivityIndicator's IsRunning property. If we do not define 
        // IsSearching as a bindable property, its initial value (false) will 
        // be used to set ActivityIndicator.IsRunning. Later when we change
        // the value of IsSearching, ActivityIndicator will be unaware of this. 
        // So, we need to implement it as a bindable property. 
        private BindableProperty IsSearchingProperty =
            BindableProperty.Create("IsSearching", typeof(bool), typeof(AdsHoundSearchPage), false);
        public bool IsSearching
        {
            get { return (bool)GetValue(IsSearchingProperty); }
            set { SetValue(IsSearchingProperty, value); }
        }

        public AdsHoundSearchPage()
        {
            BindingContext = this;

            InitializeComponent();
        }

        async void OnTextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            // Here we are checking for "null" because if the user presses Cancel, 
            // e.NewTextValue will be null. 
            if (e.NewTextValue == null || e.NewTextValue.Length < AdService.MinSearchLength)
                return;

            // Note that I've Ad the logic of setting the ListView's ItemSource
            // and handling errors to LoadAds method. The reason for this is 
            // because try/catch blocks should not be in the middle of your methods. 
            await FindAds(actor: e.NewTextValue);
        }

        async Task FindAds(string actor)
        {
            // Clean coding tip: try/catch should NOT be in the middle 
            // of a method. 

            // The responsibility of this method is quite clear. 
            // It calls the service and displays the result (either movies 
            // in the ListView or a Label). If anything unexpected happens,
            // it prevents the application from crashing and displays an alert. 
            // If we were to call this method from two different places, we 
            // wouldn't have to do error handling or setting IsSearching in
            // two different places. All these go hand-in-hand with setting
            // ListView's ItemSource. That's why I've encapsulated all that 
            // in one method. 
            try
            {
                IsSearching = true;

                var ads = await _service.FindAdsByActor(actor);
                adsListView.ItemsSource = ads;
                adsListView.IsVisible = ads.Any();
                notFound.IsVisible = !adsListView.IsVisible;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "Could not retrieve the list of ads.", "OK");
            }
            finally
            {
                IsSearching = false;
            }
        }


        async void OnAdSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;
            
            var ad = e.SelectedItem as Ad;

            adsListView.SelectedItem = null;

            await Navigation.PushAsync(new AdDetailsPage(ad));
        }
    }
}

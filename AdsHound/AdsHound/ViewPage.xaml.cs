﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AdsHound
{
    public partial class ViewPage : ContentPage
    {
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new LogAdsCuponPage());
        }

        public ViewPage()
        {
            InitializeComponent();
        }
    }
}

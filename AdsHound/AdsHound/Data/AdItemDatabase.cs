﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdsHound.Models;
using SQLite;


namespace AdsHound.Data
{
    public class AdItemDatabase
    {

        readonly SQLiteAsyncConnection database;

        public AdItemDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<AdItem>().Wait();
        }

        public Task<List<AdItem>> GetItemsAsync()
        {
            return database.Table<AdItem>().ToListAsync();
        }

        public Task<List<AdItem>> GetItemsNotDoneAsync()
        {
            return database.QueryAsync<AdItem>("SELECT * FROM [TodoItem] WHERE [Done] = 0");
        }

        public Task<AdItem> GetItemAsync(int id)
        {
            return database.Table<AdItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(AdItem item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(AdItem item)
        {
            return database.DeleteAsync(item);
        }
    }
}

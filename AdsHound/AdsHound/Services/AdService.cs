﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AdsHound.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AdsHound.Services
{
    public class AdService
    {
        public static readonly int MinSearchLength = 5;


        private const string Url = "http://javier1nc.com/developer/adshound";
        //private const string Url = "https://api.yelp.com/v3/businesses";


        private HttpClient _client = new HttpClient();


        public async Task<IEnumerable<Ad>> FindAdsByActor(string actor)
        {
            if (actor.Length < MinSearchLength)
                return Enumerable.Empty<Ad>();

            var response = await _client.GetAsync($"{Url}/search/comida");


            //var response = await _client.GetAsync($"{Url}/search?term=Food&latitude=19.332091&longitude=-99.18824");



            if (response.StatusCode == HttpStatusCode.NotFound)
                return Enumerable.Empty<Ad>();
            
            var content = await response.Content.ReadAsStringAsync();



            //JToken token = JObject.Parse(content);

            //string businesses = (string)token.SelectToken("businesses");



            return JsonConvert.DeserializeObject<List<Ad>>(content);


        }

        public async Task<Ad> GetAd(string alias)
        {
            //_client.DefaultRequestHeaders.Add("Authorization", "Bearer " + "XjD0nCy7ZbzRf4B0Jq88jAujC3Xlf5bnA_ZWBKUNG6CJ0hToG0fQtPX_Vjlj0CBMG9_xVqauW9crYHrw7zuMiHcN6pBHfp9sTtHR1mrPR7j3GmJ35dWh9pshQbBUW3Yx");

            var response = await _client.GetAsync($"{Url}/search/item");
            //var response = await _client.GetAsync($"{Url}/{alias}");

            if (response.StatusCode == HttpStatusCode.NotFound)
                return null;

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Ad>(content);
        }
    }
}
